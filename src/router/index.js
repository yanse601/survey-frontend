import Vue from "vue"
import VueRouter from "vue-router"

import userLogin from "@/views/user/userLogin"
import userTable from "@/views/user/userTable"
import userForm from "@/views/user/userForm"
import projectTable from "@/views/project/projectTable"
import projectForm from "@/views/project/projectForm"

import surveyTable from "@/views/survey/surveyTable"
import surveyDesigner from "@/views/survey/surveyDesigner"
import surveyPreview from "@/views/survey/surveyPreview"
import surveySender from "@/views/survey/surveySender"
import surveyAnswer from "@/views/survey/surveyAnswer"
import surveyResultTable from "@/views/survey/surveyResultTable"
import surveyAnalysis from "@/views/survey/surveyAnalysis"
import surveyProjectTable from "@/views/survey/surveyProjectTable"
import surveyCreation from "@/views/survey/surveyCreation"
import surveyLinkTable from "@/views/survey/surveyLinkTable"
import surveyForm from "@/views/survey/surveyForm"
import store from "../store"

Vue.use(VueRouter)
//TODO: 优化为更restful的风格
const router =new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/userLogin',
            component: userLogin,
        },
        {
            path: '/userTable',
            component: userTable,
        },
        {
            path: '/userForm',
            component: userForm,
        },
        {
            path: '/projectTable',
            component: projectTable
        },
        {
            path: '/projectForm',
            component: projectForm,
        },
        {
            path: '/surveyTable/',
            component: surveyTable,
        },
        {
            path: '/surveyAnalysis',
            component: surveyAnalysis,
        },
        {
            path: '/surveyDesigner/:purpose',
            component: surveyDesigner,
        },
        {
            path:'/surveyPreview',
            component: surveyPreview
        },
        {
            path: '/surveySender',
            component: surveySender,
        }, 
        {
            path: '/surveyAnswer',
            component:surveyAnswer,
        },
        {
            path: '/surveyResultTable',
            component: surveyResultTable,
        },
        {
            path: '/surveyProjectTable',
            component: surveyProjectTable,
        },{
            path:'/surveyCreation',
            component: surveyCreation
        },{
            path: '/surveyLinkTable',
            component: surveyLinkTable
        },{
            path:'/surveyForm',
            component: surveyForm
        },
       
        {
            path: '/',
            redirect: '/userLogin'
        }
    ]
})


router.beforeEach((to, from, next) => {
    let authed = true;
    if(store.state.userToken == ''||store.state.userToken == null){
        authed = false;
    }
    if (to.path !== '/userLogin' && !authed) next({ path: '/userLogin' })
    else next()
  })

  export default router;