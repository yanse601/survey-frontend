import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        userToken: localStorage.getItem('userToken') ? localStorage.getItem('userToken') : '',
        userInfo: JSON.parse(localStorage.getItem('userInfo') ? localStorage.getItem('userInfo') : '{}')
    },
    mutations: {
        setUserToken(state,userToken) {
            state.userToken = userToken
            localStorage.setItem("userToken",userToken)
        },
        setUserInfo(state,userInfo) {
            state.userInfo = userInfo
            localStorage.setItem("userInfo",JSON.stringify(userInfo))
        }
    }
})

export default store;